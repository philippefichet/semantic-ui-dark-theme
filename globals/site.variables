/*******************************
     User Global Variables
*******************************/

/*-------------------
        Page
--------------------*/

@pageBackground      : #333;
@textColor           : rgba(200, 200, 200, 0.87);
@linkedInColor       : #d47fff;
@linkColor           : #32b8ff;
@linkHoverColor      : lighten(@linkColor, 10);

/*-------------------
        Formulaire
--------------------*/
@inputBackground      : #444;
@mutedTextColor       : rgba(200, 200, 200, 0.87);

@greenTextColor  : #1EBC30; // Green is difficult to read

/*---  Colors  ---*/
@red              : #661c14;
@orange           : #663514;
@yellow           : #665114;
@olive            : #5c6614;
@green            : #146627;
@teal             : #146661;
@blue             : #144366;
@violet           : #2e1466;
@purple           : #511466;
@pink             : #661443;
@brown            : #663314;
@grey             : #767676;
@black            : #1B1C1D;

/*@red              : #bf948f;
@orange           : #bfa58f;
@yellow           : #bfb58f;
@olive            : #b8bf8f;
@green            : #8fbf94;
@teal             : #8fbfbf;
@blue             : #8fa8bf;
@violet           : #958fbf;
@purple           : #ad8fbf;
@pink             : #bf8fb8;
@brown            : #bfa78f;
@grey             : #767676;
@black            : #1B1C1D;*/

/*---  Light Colors  ---*/
@lightRed         : #7f2218;
@lightOrange      : #7f4218;
@lightYellow      : #7f6518;
@lightOlive       : #737f18;
@lightGreen       : #197f31;
@lightTeal        : #187f79;
@lightBlue        : #18537f;
@lightViolet      : #39187f;
@lightPurple      : #65187f;
@lightPink        : #7f1953;
@lightBrown       : #7f4019;
@lightGrey        : #7f7f7f;
@lightBlack       : #1B1C1D;

/*--- Colored Text ---*/
@redTextColor    : #bf948f;
@orangeTextColor : #bfa58f;
@yellowTextColor : #bfb58f; // Yellow text is difficult to read
@oliveTextColor  : #b8bf8f; // Olive is difficult to read
@greenTextColor  : #8fbf94; // Green is difficult to read
@tealTextColor   : #8fbfbf; // Teal text is difficult to read
@blueTextColor   : #8fa8bf;
@violetTextColor : #958fbf;
@purpleTextColor : #ad8fbf;
@pinkTextColor   : #bf8fb8;
@brownTextColor  : #bfa78f;

/*--- Colored Backgrounds ---*/
@redBackground    : #661c14;
@orangeBackground : #663514;
@yellowBackground : #665114;
@oliveBackground  : #5c6614;
@greenBackground  : #146627;
@tealBackground   : #146661;
@blueBackground   : #144366;
@violetBackground : #2e1466;
@purpleBackground : #511466;
@pinkBackground   : #661443;
@brownBackground  : #663314;

/* Positive */
@positiveColor           : @green;
@positiveBackgroundColor : #1A531B;
@positiveBorderColor     : #7bb45e;
@positiveHeaderColor     : #c1cda5;
@positiveTextColor       : #86b26f;

/* Negative */
@negativeColor           : @red;
@negativeBackgroundColor : #912D2B;
@negativeBorderColor     : #c58c8c;
@negativeHeaderColor     : #dcb5b5;
@negativeTextColor       : #efb6b5;

/* Info */
@infoColor              : #31CCEC;
@infoBackgroundColor    : #0E566C;
@infoBorderColor        : #76aeb9;
@infoHeaderColor        : #b3d8d8;
@infoTextColor          : #A9D5DE;

/* Warning */
@warningColor           : #B8932F;
@warningBackgroundColor : #573A08;
@warningBorderColor     : #F2C037;
@warningHeaderColor     : #F2C037;
@warningTextColor       : #b8932f;

/*---  Colors  ---*/
@primaryColorDown    : darken(@primaryColor, 10);
@secondaryColorDown  : lighten(@secondaryColor, 10);

/*@redDown             : darken(@redBackground, 10);
@orangeDown          : darken(@orangeBackground, 10);
@yellowDown          : darken(@yellowBackground, 10);
@oliveDown           : darken(@oliveBackground, 10);
@greenDown           : darken(@greenBackground, 10);
@tealDown            : darken(@tealBackground, 10);
@blueDown            : darken(@blueBackground, 10);
@violetDown          : darken(@violetBackground, 10);
@purpleDown          : darken(@purpleBackground, 10);
@pinkDown            : darken(@pinkBackground, 10);
@brownDown           : darken(@brownBackground, 10);

@redHover             : saturate(darken(@redBackground, 5), 10, relative);
@orangeHover          : saturate(darken(@orangeBackground, 5), 10, relative);
@yellowHover          : saturate(darken(@yellowBackground, 5), 10, relative);
@oliveHover           : saturate(darken(@oliveBackground, 5), 10, relative);
@greenHover           : saturate(darken(@greenBackground, 5), 10, relative);
@tealHover            : saturate(darken(@tealBackground, 5), 10, relative);
@blueHover            : saturate(darken(@blueBackground, 5), 10, relative);
@violetHover          : saturate(darken(@violetBackground, 5), 10, relative);
@purpleHover          : saturate(darken(@purpleBackground, 5), 10, relative);
@pinkHover            : saturate(darken(@pinkBackground, 5), 10, relative);
@brownHover           : saturate(darken(@brownBackground, 5), 10, relative);

@redFocus             : saturate(darken(@redBackground, 8), 20, relative);
@orangeFocus          : saturate(darken(@orangeBackground, 8), 20, relative);
@yellowFocus          : saturate(darken(@yellowBackground, 8), 20, relative);
@oliveFocus           : saturate(darken(@oliveBackground, 8), 20, relative);
@greenFocus           : saturate(darken(@greenBackground, 8), 20, relative);
@tealFocus            : saturate(darken(@tealBackground, 8), 20, relative);
@blueFocus            : saturate(darken(@blueBackground, 8), 20, relative);
@violetFocus          : saturate(darken(@violetBackground, 8), 20, relative);
@purpleFocus          : saturate(darken(@purpleBackground, 8), 20, relative);
@pinkFocus            : saturate(darken(@pinkBackground, 8), 20, relative);
@brownFocus           : saturate(darken(@brownBackground, 8), 20, relative);

@lightRedDown        : darken(@lightRed, 5);
@lightOrangeDown     : darken(@lightOrange, 5);
@lightYellowDown     : darken(@lightYellow, 5);
@lightOliveDown      : darken(@lightOlive, 5);
@lightGreenDown      : darken(@lightGreen, 5);
@lightTealDown       : darken(@lightTeal, 5);
@lightBlueDown       : darken(@lightBlue, 5);
@lightVioletDown     : darken(@lightViolet, 5);
@lightPurpleDown     : darken(@lightPurple, 5);
@lightPinkDown       : darken(@lightPink, 5);
@lightBrownDown      : darken(@lightBrown, 5);
@lightGreyDown       : darken(@lightGrey, 5);
@lightBlackDown      : darken(@fullBlack, 5);*/

/*-------------------
    Brand Colors
--------------------*/

@primaryColor        : @blueBackground;
@secondaryColor      : @black;

@lightPrimaryColor   : @lightBlue;
@lightSecondaryColor : @lightBlack;

/*-------------------
   Inverted Colors   
--------------------*/
