# Semantic-UI dark Theme

## Prepare sources

```sh
git clone https://github.com/Semantic-Org/Semantic-UI
cd Semantic-UI
git submodule add https://philippefichet@bitbucket.org/philippefichet/semantic-ui-dark-theme.git src/themes/dark-default
```

## Change src/theme.config with :

copy Semantic-UI/src/themes/dark-default/theme.config to Semantic-UI/theme.config

## Compile

```sh
gulp build
```

## How to Use

Copy `dist` folder and use `semantic.css` (or `semantic.min.css`) and `semantic.js` (or `semantic.min.js`)