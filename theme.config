/*

████████╗██╗  ██╗███████╗███╗   ███╗███████╗███████╗
╚══██╔══╝██║  ██║██╔════╝████╗ ████║██╔════╝██╔════╝
   ██║   ███████║█████╗  ██╔████╔██║█████╗  ███████╗
   ██║   ██╔══██║██╔══╝  ██║╚██╔╝██║██╔══╝  ╚════██║
   ██║   ██║  ██║███████╗██║ ╚═╝ ██║███████╗███████║
   ╚═╝   ╚═╝  ╚═╝╚══════╝╚═╝     ╚═╝╚══════╝╚══════╝

*/

/*******************************
        Theme Selection
*******************************/

/* To override a theme for an individual element
   specify theme name below
*/
@themeName: 'dark-default';
/* Global */
@site       : @themeName;
@reset      : @themeName;

/* Elements */
@button     : @themeName;
@container  : @themeName;
@divider    : @themeName;
@flag       : @themeName;
@header     : @themeName;
@icon       : 'default';
@image      : @themeName;
@input      : @themeName;
@label      : @themeName;
@list       : @themeName;
@loader     : @themeName;
@rail       : @themeName;
@reveal     : @themeName;
@segment    : @themeName;
@step       : @themeName;

/* Collections */
@breadcrumb : @themeName;
@form       : @themeName;
@grid       : @themeName;
@menu       : @themeName;
@message    : @themeName;
@table      : @themeName;

/* Modules */
@accordion  : 'default';
@checkbox   : 'default';
@dimmer     : 'default';
@dropdown   : 'default';
@embed      : 'default';
@modal      : 'default';
@nag        : 'default';
@popup      : 'default';
@progress   : 'default';
@rating     : 'default';
@search     : 'default';
@shape      : 'default';
@sidebar    : 'default';
@sticky     : 'default';
@tab        : 'default';
@transition : 'default';

/* Views */
@ad         : 'default';
@card       : 'default';
@comment    : 'default';
@feed       : 'default';
@item       : 'default';
@statistic  : 'default';

/*******************************
            Folders
*******************************/

/* Path to theme packages */
@themesFolder : 'themes';

/* Path to site override folder */
@siteFolder   : 'site/';


/*******************************
         Import Theme
*******************************/

@import "theme.less";

/* End Config */